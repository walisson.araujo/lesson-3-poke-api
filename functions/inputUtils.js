import promptSync from 'prompt-sync';

export function requestTypeOfPokemon() {
    const prompt = promptSync();
    return prompt("Insert your Pokemon type here: ");

}

export function requestNameOfPokemon() {
    const prompt = promptSync();
    return prompt("Insert your pokemon name here: ");

}
export function isCrescentOrDecrescent() {
    const prompt = promptSync();
    return prompt("Insert crescent or decrescent");

}

export function formatNewPokemon(){ 
    const prompt = promptSync();
    const pokemonName = prompt("Insert your new pokemon name here: ");
    const pokemonNumber = prompt("Insert your new Pokemon number here: ");
    const pokemonURL = "https://pokeapi.co/api/v2/pokemon/"+pokemonNumber+"/";
    
    return {pokemonName, pokemonURL};

}