import { getCommonPokemonInLists, addPokemon, removePokemon, sortByName,
        editPokemonName, getPokemonOnList, concatTwoLists} 
from "./listPokemonController.js";
import { requestURL, getListOfPokemonFromURL } from "./consumePokeAPI.js";
import { requestTypeOfPokemon, requestNameOfPokemon, isCrescentOrDecrescent } from "./inputUtils.js";

export {
    getCommonPokemonInLists, getListOfPokemonFromURL, requestURL,
    addPokemon, removePokemon, requestNameOfPokemon, isCrescentOrDecrescent,
    requestTypeOfPokemon, sortByName, editPokemonName, getPokemonOnList,
    concatTwoLists
}