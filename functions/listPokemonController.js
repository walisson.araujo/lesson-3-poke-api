
export function getPokemonOnList(listOfPokemon, name) {
    let pokemonOnListURL = '';
    let pokemonOnListNumber;
    let pokemonOnListIndex = 0;
    let pokemonOnListFound = false;
    listOfPokemon.every(pokemon => {
        pokemonOnListIndex++;
        if (pokemon.name == name) {
            pokemonOnListURL = pokemon.url;
            pokemonOnListNumber = pokemon.url.slice(34, pokemonOnListURL.length - 1);
            pokemonOnListFound = true;
        } else {
            pokemonOnListURL = 'This pokemon is not on the list';
        }

        return pokemon.name !== name;
    });
    return { pokemonOnListURL, pokemonOnListNumber, pokemonOnListIndex, pokemonOnListFound };
}

export function addPokemon(pokemon, listOfPokemon) {
    if (!getPokemonOnList(listOfPokemon, pokemon.pokemonName).pokemonOnListFound) {
        listOfPokemon.push(pokemon);
    } else {
        console.log("This operation would fail because the list of Pokemon already contains this pokemon");
    }
    return listOfPokemon;
}

export function removePokemon(pokemonName, listOfPokemon) {
    let newListOfPokemon;
    if (getPokemonOnList(listOfPokemon, pokemonName).pokemonOnListFound) {
        newListOfPokemon = listOfPokemon.filter(element => element.name !== pokemonName);

    } else {
        console.log("This operation fail because the list does not contain the Pokemon");
    }
    return newListOfPokemon;
}

export function editPokemonName(pokemonName, newPokemonName,listOfPokemon) {
    if (getPokemonOnList(listOfPokemon, pokemonName).pokemonOnListFound) {
        listOfPokemon = listOfPokemon.every(
            pokemon =>{
            if (pokemon.name == pokemonName) {
                pokemon.name = newPokemonName;
                
            } else {
                console.log('This pokemon is not on the list');
            }
    
            return pokemon.name !== pokemonName;
    });

    } else {
        console.log("This operation fail because the list does not contain the Pokemon");
    }
    return listOfPokemon;
}

export function sortByName (listOfPokemon, isCrescent) {
    if (isCrescent == "crescent") {
        listOfPokemon = listOfPokemon.sort((a, b) => {
            let x = a.name.toLowerCase();
            let y = b.name.toLowerCase();
            return x == b ? 0: x < y ? 1 : -1;   
        });
    } else if (isCrescent == "decrescent") {
        listOfPokemon = listOfPokemon.sort((a,b) =>{ 
            if (a>b)
                return -1;
            if (a<b)
                return 1;
            return 0;
        });
    }
    return listOfPokemon;
}

export function getCommonPokemonInLists(listOfPokemon1, listOfPokemon2) {
    let listOfPokemon3=[];
    listOfPokemon1.forEach(pokemon1 => {
        listOfPokemon2.forEach(pokemon2 => {
            if (pokemon1.name == pokemon2.name){
                listOfPokemon3.push(pokemon2);
            }
        });
    });
    return listOfPokemon3;
}

export function concatTwoLists (listOfPokemon1, listOfPokemon2){
    Array.prototype.push.apply(listOfPokemon1, listOfPokemon2);
    return listOfPokemon1;
    
}