import fetch from "node-fetch";

export function requestURL(type) {
    return "https://pokeapi.co/api/v2/type/" + type;
}

export async function getListOfPokemonFromURL(url) {
    let myListPokemon = [];
    await fetch(url)
        .then(response => response.json())
        .then(data => data.pokemon.forEach(pokemon => {
            myListPokemon.push(pokemon.pokemon);
        }));
    return myListPokemon;

}
