import {requestTypeOfPokemon, requestURL, getListOfPokemonFromURL, concatTwoLists} 
from "../functions/index.js";

console.log("Insert the first pokemon type list");
let typeOf1stList = requestTypeOfPokemon();
let urlOf1stList = requestURL(typeOf1stList);
console.log("Insert the second pokemon type list");
let typeOf2ndList = requestTypeOfPokemon();
let urlOf2ndList = requestURL(typeOf2ndList);
let listOfPokemon1 = await getListOfPokemonFromURL(urlOf1stList);
let listOfPokemon2 = await getListOfPokemonFromURL(urlOf2ndList);
console.log(concatTwoLists(listOfPokemon1, listOfPokemon2));