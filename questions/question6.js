import { getListOfPokemonFromURL, requestURL, sortByName, requestTypeOfPokemon} 
from "../functions/index.js";   

let type = requestTypeOfPokemon();
let url = requestURL(type);
let listOfPokemon = await getListOfPokemonFromURL(url);
let isCrescent = isCrescentOrDecrescent();
listOfPokemon = sortByName(listOfPokemon, isCrescent);
console.log(listOfPokemon);