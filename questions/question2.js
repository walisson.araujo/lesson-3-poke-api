import { getListOfPokemonFromURL, requestURL, requestTypeOfPokemon, getPokemonOnList, requestNameOfPokemon}
from "../functions/index.js";
let type = requestTypeOfPokemon();
let url = requestURL(type);
let listOfPokemon = await getListOfPokemonFromURL(url);
let nameOfPokemon = requestNameOfPokemon();
let searchPokemonOnList = getPokemonOnList(listOfPokemon, nameOfPokemon);
console.log("URL of pokemon: " + searchPokemonOnList.pokemonOnListURL+
            ", \nnumber of Pokemon: " + searchPokemonOnList.pokemonOnListNumber+
            ", \nindex: " + searchPokemonOnList.pokemonOnListIndex);
