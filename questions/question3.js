import { getListOfPokemonFromURL, requestURL, requestTypeOfPokemon, formatNewPokemon, addPokemon}
from "../functions/index.js";
let type = requestTypeOfPokemon();
let url = requestURL(type);
let listOfPokemon = await getListOfPokemonFromURL(url);
let pokemon = formatNewPokemon();
console.log(addPokemon(pokemon, listOfPokemon));
