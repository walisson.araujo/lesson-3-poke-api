import { editPokemonName, getListOfPokemonFromURL, requestURL, requestTypeOfPokemon, requestNameOfPokemon} 
from "../functions/index.js";   
         
let type = requestTypeOfPokemon();
let url = requestURL(type);
let listOfPokemon = await getListOfPokemonFromURL(url);
let pokemonName = requestNameOfPokemon();
let newPokemonName = requestNameOfPokemon();
listOfPokemon = editPokemonName(pokemonName, newPokemonName, listOfPokemon);
console.log(listOfPokemon);