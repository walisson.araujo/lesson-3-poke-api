import { getListOfPokemonFromURL, requestURL, requestTypeOfPokemon, requestNameOfPokemon, removePokemon}
from "../functions/index.js";   
let type = requestTypeOfPokemon();
let url = requestURL(type);
let listOfPokemon = await getListOfPokemonFromURL(url);
let pokemon = requestNameOfPokemon();
listOfPokemon = removePokemon(pokemon, listOfPokemon);
console.log(listOfPokemon);