import { getListOfPokemonFromURL, requestURL, requestTypeOfPokemon}
from "../functions/index.js";
let type = requestTypeOfPokemon();
let url = requestURL(type);
console.log(await getListOfPokemonFromURL(url));